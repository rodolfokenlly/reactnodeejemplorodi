const mongoose = require('mongoose');
const log = require('../src/helpers/color__console');
const URI = 'mongodb://localhost/mern-crud-test';

const options = { useNewUrlParser: true, useCreateIndex: true }

mongoose.connect(URI, options)
  .then(db => log.success('DB is connected'))
  .catch(error => log.error('Error : ', error));

module.exports = mongoose;