const express = require('express');
const morgan = require('morgan');
const path = require('path');
const log = require('../src/helpers/color__console');

const app = express();

const { mongoose } = require('./database');

app.set('port', process.env.PORT || 3000);

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/films', require('./routes/film.routes'));
app.use('/api/users', require('./routes/user.routes'));

app.use(express.static(path.join(__dirname, 'public')));;

app.listen(app.get('port'), () => {
  log.info(`Server on port ${app.get('port')}`);
});