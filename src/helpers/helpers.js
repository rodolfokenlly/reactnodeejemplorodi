//#region Requerimiento de Variables

const stringify = require('json-stringify');
const dateFormat = require('dateformat');

//#endregion Requerimiento de Variables

//#region Funciones de Obtencion de Fechas

const GetTimeActual = () => {//DEVUELVE NUMERO
    return new Date().getTime();
}

const GetFechaHoraActual = () => {//DEVUELVE LA FECHA EN STRING
    return dateFormat(new Date(), "dd/mm/yyyy HH:MM:ss");
}

const GetDateUTC = () => {//DEVUELVE LA FECHA EN DATE
    let d = new Date();
    return new Date(
        Date.UTC(
            d.getFullYear(),//ANIO
            d.getMonth(),//MES
            d.getDate(),//DIA
            d.getHours(),//HORA
            d.getMinutes(),//MINUTO
            d.getSeconds()//SEGUNDO
        )
    );
}

const getDateISO = () => {
    let FechaActual = new Date();

    let MesActual = (FechaActual.getMonth() + 1);
    if (MesActual < 10) {
        MesActual = '0' + MesActual;
    }

    let DiaActual = FechaActual.getDate();
    if (DiaActual < 10) {
        DiaActual = '0' + DiaActual;
    }

    let HoraActual = FechaActual.getHours();
    if (HoraActual < 10) {
        HoraActual = '0' + HoraActual;
    }

    let MinutoActual = FechaActual.getMinutes();
    if (MinutoActual < 10) {
        MinutoActual = '0' + MinutoActual;
    }

    let SegundoActual = FechaActual.getSeconds();
    if (SegundoActual < 10) {
        SegundoActual = '0' + SegundoActual;
    }

    let Fecha_Registro = FechaActual.getFullYear() + '-' + MesActual + '-' + DiaActual + ' ' + HoraActual + ':' + MinutoActual + ':' + SegundoActual;

    return Fecha_Registro;
}

const GetFechaISOEnviada = (Fecha, Tipo) => {
    let Dia = Fecha.toString().substr(0, 2);
    let Mes = Fecha.toString().substr(3, 2);
    let Anio = Fecha.toString().substr(6, 4);
    let HoraActual = '';
    let MinutoActual = '';

    if (Tipo == 1) {
        HoraActual = '00';
        MinutoActual = '00';
    }
    else if (Tipo == 2) {
        HoraActual = '23';
        MinutoActual = '59';
    }
    else {
        HoraActual = Fecha.toString().substr(11, 2);
        MinutoActual = Fecha.toString().substr(14, 2);
    }

    let Fecha_Registro = Anio + '-' + Mes + '-' + Dia + ' ' + HoraActual + ':' + MinutoActual;

    return Fecha_Registro;
}

const ObtenerFechaFinAnio = () => {
    let FechaActual = new Date();
    let Mes = 12;
    let Dia = 31;
    let Hora = 23;
    let Minuto = 59;

    let Fecha = FechaActual.getFullYear() + '-' + Mes + '-' + Dia + ' ' + Hora + ':' + Minuto;

    return Fecha;
}

//#endregion Funciones de Obtencion de Fechas

//#region Funciones de Obtencion de Id Fechas

const ObtenerIdFecha = (Fecha) => {
    let Dia = parseInt(Fecha.toString().substr(0, 2));
    let Mes = parseInt(Fecha.toString().substr(3, 2)) * 100;
    let Anio = parseInt(Fecha.toString().substr(6, 4)) * 10000;

    return (Anio + Mes + Dia);
}

const getIdDate = () => {
    let FechaActual = new Date();

    let AnioActual = FechaActual.getFullYear() * 10000;

    let MesActual = (FechaActual.getMonth() + 1);
    MesActual = MesActual * 100;

    let DiaActual = FechaActual.getDate();
    DiaActual = DiaActual;

    return (AnioActual + MesActual + DiaActual);
}

const ObtenerIdFechaCompletoActual = () => {
    let FechaActual = new Date();

    let AnioActual = FechaActual.getFullYear() * 10000000000;

    let MesActual = (FechaActual.getMonth() + 1);
    MesActual = MesActual * 100000000;

    let DiaActual = FechaActual.getDate();
    DiaActual = DiaActual * 1000000;

    let HoraActual = FechaActual.getHours();
    HoraActual = HoraActual * 10000;

    let MinutoActual = FechaActual.getMinutes();
    MinutoActual = MinutoActual * 100;

    let SegundoActual = FechaActual.getSeconds();

    return (AnioActual + MesActual + DiaActual + HoraActual + MinutoActual + SegundoActual);
}

const ObtenerIdFechaISO = (Fecha) => {
    let Anio = parseInt(Fecha.toString().substr(0, 4)) * 10000;
    let Mes = parseInt(Fecha.toString().substr(5, 2)) * 100;
    let Dia = parseInt(Fecha.toString().substr(8, 2));

    return (Anio + Mes + Dia);
}

const ObtenerHoraMinutoSegundo = () => {
    let FechaActual = new Date();

    let Hora = FechaActual.getHours() * 10000;
    let Minuto = (FechaActual.getMinutes()) * 100;
    let Segundo = FechaActual.getSeconds();

    return (Hora + Minuto + Segundo);
}
//#endregion Funciones de Obtencion de Id Fechas

//#region Funciones Generales

const ConvertFormatJSON = (Tabla, PrimaryKey, callback) => {
    var TablaRespuesta = [];
    var Completado = 0;
    var LlavesArray = [];
    var LlavesArray2 = [];

    do {
        if (Completado == 0) {
            Completado = 1;
            for (var i = 0; i < Tabla.length; i++) {
                delete Tabla[i]._id;
                var KeysFila = Object.keys(Tabla[i]);
                for (j = 0; j < KeysFila.length; j++) {

                    if (KeysFila[j] == PrimaryKey) {
                        var Dato = parseInt(Tabla[i][KeysFila[j]]);
                        Tabla[i][KeysFila[j]] = Dato;
                        continue;
                    }

                    if (typeof Tabla[i][KeysFila[j]] == 'string') {
                        var Dato = Tabla[i][KeysFila[j]];
                        Dato = Dato.toString();

                        if (Dato.substr(0, 1) == '[' || Dato.substr(0, 1) == '{') {
                            Dato = JSON.parse(Dato);
                            Tabla[i][KeysFila[j]] = Dato;

                            if (LlavesArray.length < 1) {
                                var Elemento = {
                                    Key: KeysFila[j],
                                    Parent: []
                                };
                                LlavesArray.push(Elemento);
                            }
                            else {
                                var Repetido = false;
                                LlavesArray.map(function (elemento) {
                                    if (elemento.Key == KeysFila[j]) {
                                        Repetido = true;
                                    }
                                });

                                if (!Repetido) {
                                    var Elemento = {
                                        Key: KeysFila[j],
                                        Parent: []
                                    };
                                    LlavesArray.push(Elemento);
                                }
                            }
                        }
                    }
                }
            }

        }
        else {
            LlavesArray.map(function (elemento) {
                // console.log(elemento);
                if (elemento.Parent.length < 1) {
                    for (var i = 0; i < Tabla.length; i++) {
                        var Fila = Tabla[i];
                        var SubTabla = Fila[elemento.Key];

                        if (SubTabla == undefined) {
                            continue;
                        }

                        for (var k = 0; k < SubTabla.length; k++) {
                            var KeysFila = Object.keys(SubTabla[k]);

                            for (var j = 0; j < KeysFila.length; j++) {

                                if (typeof SubTabla[k][KeysFila[j]] == 'string') {
                                    var Dato = SubTabla[k][KeysFila[j]];

                                    Dato = Dato.toString();

                                    if (Dato.substr(0, 1) == '[' || Dato.substr(0, 1) == '{') {
                                        Dato = JSON.parse(Dato);
                                        SubTabla[k][KeysFila[j]] = Dato;
                                        if (LlavesArray2.length < 1) {
                                            var Padres = [];
                                            elemento.Parent.map(function (a) {
                                                Padres.push(a);
                                            });
                                            Padres.push({ Name: elemento.Key });
                                            var Elemento2 = {
                                                Key: KeysFila[j],
                                                Parent: Padres
                                            };
                                            LlavesArray2.push(Elemento2);
                                        }
                                        else {
                                            var Repetido = false;
                                            LlavesArray2.map(function (elemento3) {
                                                if (elemento3.Key == KeysFila[j]) {
                                                    Repetido = true;
                                                }
                                            });

                                            if (!Repetido) {
                                                var Padres = [];
                                                elemento.Parent.map(function (a) {
                                                    Padres.push(a);
                                                });
                                                Padres.push({ Name: elemento.Key });
                                                var Elemento2 = {
                                                    Key: KeysFila[j],
                                                    Parent: Padres
                                                };
                                                LlavesArray2.push(Elemento2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {

                    for (var i = 0; i < Tabla.length; i++) {

                        var Fila = Tabla[i];
                        var Fila2 = [];
                        for (var l = 0; l < elemento.Parent.length; l++) {
                            Fila = Fila[elemento.Parent[l].Name];
                        }

                        var SubTabla = Fila;

                        if (SubTabla == undefined) {
                            continue;
                        }
                        // console.log(SubTabla);
                        for (var p = 0; p < SubTabla.length; p++) {
                            var Elementos = SubTabla[p][elemento.Key];

                            if (Elementos == undefined) {
                                continue;
                            }

                            for (var k = 0; k < Elementos.length; k++) {
                                var KeysFila = Object.keys(Elementos[k]);

                                for (var j = 0; j < KeysFila.length; j++) {
                                    if (typeof Elementos[k][KeysFila[j]] == 'string') {
                                        var Dato = Elementos[k][KeysFila[j]];

                                        Dato = Dato.toString();

                                        if (Dato.substr(0, 1) == '[' || Dato.substr(0, 1) == '{') {
                                            Dato = JSON.parse(Dato);
                                            Elementos[k][KeysFila[j]] = Dato;
                                            if (LlavesArray2.length < 1) {
                                                var Padres = [];
                                                elemento.Parent.map(function (a) {
                                                    Padres.push(a);
                                                });
                                                Padres.push({ Name: elemento.Key });
                                                var Elemento2 = {
                                                    Key: KeysFila[j],
                                                    Parent: Padres
                                                };
                                                LlavesArray2.push(Elemento2);
                                            }
                                            else {
                                                var Repetido = false;
                                                LlavesArray2.map(function (elemento3) {
                                                    if (elemento3.Key == KeysFila[j]) {
                                                        Repetido = true;
                                                    }
                                                });

                                                if (!Repetido) {
                                                    var Padres = [];
                                                    elemento.Parent.map(function (a) {
                                                        Padres.push(a);
                                                    });
                                                    Padres.push({ Name: elemento.Key });
                                                    var Elemento2 = {
                                                        Key: KeysFila[j],
                                                        Parent: Padres
                                                    };
                                                    LlavesArray2.push(Elemento2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            LlavesArray = LlavesArray2;
            LlavesArray2 = [];
        }
        var Cantidad = LlavesArray.length;
        // console.log(stringify(LlavesArray));
    }
    while (Cantidad > 0);

    callback(stringify(Tabla));

}

const GetCadenaAleatoria = (longitud) => {
    let caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
    let cadena = "";
    for (let i = 0; i < longitud; i++) cadena += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
    return cadena;
}

const ObtenerURIMongoDB = (user = "", pwd = "", ServerMongo = { Hosts: [], Opciones: {} }) => {
    let URI = 'mongodb://' + (user === '' || pwd === '' ? '' : user + ':' + pwd + "@");

    if (ServerMongo.Hosts.length > 1) {
        for (let i = 0; i < ServerMongo.Hosts.length; i++) {
            if (i === 0) {
                URI += ServerMongo.Hosts[i];
            }
            else if (i !== ServerMongo.Hosts.length - 1) {
                URI += ',' + ServerMongo.Hosts[i];
            }
            else {
                URI += ',' + ServerMongo.Hosts[i] + "/?";
            }
        }
    }
    else {
        URI += ServerMongo.Hosts[0] + "/?";
    }

    let Claves = Object.keys(ServerMongo.Opciones);
    for (let i = 0; i < Claves.length; i++) {
        if (i === 0) {
            URI += Claves[i] + '=' + ServerMongo.Opciones[Claves[i]];
        }
        else {
            URI += '&' + Claves[i] + '=' + ServerMongo.Opciones[Claves[i]];
        }
    }

    return URI;
}

//#endregion Funciones Generales

//#region Modulo de exportacion

module.exports = {
    GetTimeActual,
    GetFechaHoraActual,
    GetDateUTC,
    getDateISO,
    GetFechaISOEnviada,
    ConvertFormatJSON,
    getIdDate,
    ObtenerIdFechaCompletoActual,
    ObtenerFechaFinAnio,
    ObtenerIdFechaISO,
    GetCadenaAleatoria,
    ObtenerHoraMinutoSegundo,
    ObtenerURIMongoDB
}

//#endregion Modulo de exportacion
