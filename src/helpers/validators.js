const Joi = require('joi');

const shemaFilm = Joi.object().keys({
    Id: Joi.number().integer().min(0).max(100).required(),
    Title: Joi.string().uppercase().required(),
    Rating: Joi.number().integer().min(0).max(6).required(),
    Description: Joi.string().uppercase().required(),
    Summary: Joi.string().uppercase().required(),
    Type: Joi.object().keys({
        IdType: Joi.number().integer().min(1).max(10),
        Type: Joi.string().uppercase().required()
    }),
    Category: Joi.object().keys({
        IdCategory: Joi.number().integer().min(1).max(10).required(),
        Category: Joi.string().uppercase().required()
    }),
    Duration: Joi.object().keys({
        Hours: Joi.number().integer().min(0).max(10).required(),
        Minutes: Joi.string().uppercase().required()
    }),
    ReleaseYear: Joi.number().integer().min(1910).max(2025).required(),
    Serie: Joi.boolean().required(),
    Seasons: Joi.array().required(),
    Productors: Joi.array().required(),
    ProductorsExecutives: Joi.array().required(),
    Director: Joi.string().uppercase().required(),
    Creators: Joi.array().required(),
    Cast: Joi.array().required(),
    Country: Joi.string().uppercase().required(),
    ProductionCompany: Joi.string().uppercase().required(),
    Dates: Joi.object().keys({
        Registration: Joi.object().keys({
            Id: Joi.number().integer().required(),
            Date: Joi.string().uppercase().required()
        }),
        Modify: Joi.object().keys({
            Id: Joi.number().integer().required(),
            Date: Joi.string().uppercase().required()
        })
    }),
    Users: Joi.object().keys({
        Registration: Joi.object().keys({
            Id: Joi.number().integer().required(),
            User: Joi.string().uppercase().required()
        }),
        Modify: Joi.object().keys({
            Id: Joi.number().integer().required(),
            User: Joi.string().uppercase().required()
        })
    }),
    State: Joi.object().keys({
        Id: Joi.number().integer().min(1).max(10).required(),
        State: Joi.string().uppercase().required()
    })
});

const shemaUserNew = Joi.object().keys({
    IdUser: Joi.number().integer().min(0).max(100),
    Username: Joi.string().alphanum().min(3).max(30).required(),
    Email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    Password: Joi.string().regex(/^(?=.*\d)(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ]/).required(),
    Firstname: Joi.string().uppercase().required(),
    Lastname: Joi.string().uppercase().required(),
    Facebook: Joi.string().uri(),
    Dates: Joi.object().keys({
        Registration: Joi.object().keys({
            Id: Joi.number().integer().required(),
            Date: Joi.string().uppercase().required()
        }),
        Modify: Joi.object().keys({
            Id: Joi.number().integer().required(),
            Date: Joi.string().uppercase().required()
        })
    }),
    Users: Joi.object().keys({
        Registration: Joi.object().keys({
            Id: Joi.number().integer().required(),
            User: Joi.string().uppercase().required()
        }),
        Modify: Joi.object().keys({
            Id: Joi.number().integer().required(),
            User: Joi.string().uppercase().required()
        })
    }),
    State: Joi.object().keys({
        IdState: Joi.number().integer().min(1).max(10).required(),
        State: Joi.string().uppercase().required()
    })
});

const shemaUserExist = Joi.object().keys({
    UserName: Joi.string().alphanum().min(3).max(30),
    Email: Joi.string().email({ minDomainAtoms: 2 }),
    Password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    Access_token: [Joi.string(), Joi.number()],
});

module.exports = {
    shemaFilm,
    shemaUserNew,
    shemaUserExist
}