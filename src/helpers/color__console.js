const chalk = require('chalk');
const log = console.log;

const error = (text) => {
  log(chalk.red.bgBlack.bold(`[Application] - ${text}`));
}

const success = (text) => {
  log(chalk.green.bgBlack.bold(`[Application] - ${text}`));
}

const info = (text) => {
  log(chalk.blue.bgBlack.bold(`[Application] - ${text}`));
}

const warning = (text) => {
  log(chalk.yellow.bgBlack.bold(`[Application] - ${text}`));
}

module.exports = {
  error,
  success,
  info,
  warning
}
