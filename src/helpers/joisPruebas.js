const Joi = require('joi');

const shema = Joi.object().keys({
    IdUser: Joi.number().integer().min(2000).max(4000),
    User: Joi.string().alphanum().min(4).max(6).required()
});

if (Joi.validate({ IdUser: 500, User: "rodisc" }, shema).error) {
    console.log('Ocurrio un error!');
}