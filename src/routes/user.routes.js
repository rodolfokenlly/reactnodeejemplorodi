const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const validator = require('../helpers/validators');
const log = require('../helpers/color__console');
const globalfunctions = require('../helpers/helpers');
const Joi = require('joi');

router.get('*', async (req, res) => {
  try {
    log.warning('Can not find the specified route');
    res.json({
      status: 10,
      error: 'Can not find the specified route'
    });
  } catch (error) {
    log.error(`The following error occurred: ${error.message}`);
    res.json({
      status: 10,
      error: error.message
    });
  }
});

router.post('/register', async (req, res) => {
  try {
    log.info('...Running Test [INSERT USER]...');

    const responseValidate = Joi.validate(req.body, validator.shemaUserNew);
    if (responseValidate.error) {
      log.error('The validation of the data showed an error');

      return res.json({
        status: 10,
        error: 'The validation of the data showed an error'
      });
    }

    const { IdUser, Username, Email, Password, Firstname, Lastname,
      Facebook, Dates, Users, State
    } = req.body;

    const existUser = await User.find().or([{ Username }, { Email }]);
    if (existUser.length > 0) {
      log.warning('The user is already registered');

      return res.json({
        status: 10,
        error: 'The user is already registered'
      });
    }

    const resultId = await User.find().select('-_id IdUser').sort({IdUser: -1}).limit(1);
    const newIdUser = (resultId.length === 0) ? 1 : parseInt(resultId[0].IdUser + 1);

    const user = new User({ IdUser , Username, Email, Password, Firstname, Lastname,
      Facebook, Dates, Users, State
    });

    user.IdUser = newIdUser;
    user.Password = bcrypt.hashSync(Password, 10);
    user.Dates.Registration.Date = globalfunctions.getDateISO();
    user.Dates.Registration.Id = globalfunctions.getIdDate();

    const responseUserCreate = await user.save();

    if (!responseUserCreate) {
      log.error('An error occurred when registering user');

      return res.json({
        status: 10,
        error: 'An error occurred when registering user'
      });
    }

    log.success('User successfully registered');
    res.json({
      status: 1,
      data: 'User successfully registered'
    });

  } catch (error) {
    log.error(`The following error occurred: ${error.message}`);
    res.json({
      status: 10,
      error: error
    });
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { username, password, hash } = req.body;

    const user = await User.findById(id);
    const existUsername = await User.findOne({ username }).select('-_id username');

    if (!user) {
      return res.json({
        status: 10,
        error: 'User not found'
      });
    }

    if (user.username !== username && existUsername) {
      return res.json({
        status: 10,
        error: `Username ${username} is already taken`
      });
    }

    if (password) {
      hash = bcrypt.hashSync(password, 10);
    }

    user.name = username
    user.hash = hash;

    const responseUpdateUser = await user.save();

    if (!responseUpdateUser) {
      return res.json({
        status: 10,
        error: 'An error occurred while updating the user'
      });
    }

    res.json({
      status: 1,
      data: 'Successfully updated user'
    });

  } catch (error) {
    res.json({
      status: 10,
      error: error
    });
  }
})

module.exports = router;