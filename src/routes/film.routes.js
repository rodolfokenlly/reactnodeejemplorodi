const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const validator = require('../helpers/validators');
const log = require('../helpers/color__console');
const Joi = require('joi');
const Film = require('../models/film');
const globalfunctions = require('../helpers/helpers');

router.get('*', async (req, res) => {
  try {
    log.warning('Can not find the specified route');
    res.json({
      status: 10,
      error: 'Can not find the specified route'
    });
  } catch (error) {
    log.error(`The following error occurred: ${error.message}`);
    res.json({
      status: 10,
      error: error.message
    });
  }
});

router.get('/', async (req, res) => {
  try {
    const nofields = '-__v';
    const films = await Film.find().select(nofields);

    if (!films) {
      log.error('An error occurred when obtaining the data');

      return res.json({
        status: 10,
        error: 'An error occurred when obtaining the data'
      });
    }

    log.success('The data was sent correctly to the client');
    res.json({
      status: 1,
      data: films
    });

  } catch (error) {
    log.error(`The following error occurred: ${error.message}`);
    res.json({
      status: 10,
      error: error.message
    });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const task = await Task.findById(id);
    res.json({
      status: 1,
      data: task
    });
  } catch (error) {
    res.json({
      status: 10,
      error: error
    });
  }
});

router.post('/', async (req, res) => {
  try {
    log.info('...Running Test [INSERT FILM]...');

    const responseValidate = Joi.validate(req.body, validator.shemaFilm);
    if (responseValidate.error) {
      log.error('The validation of the data showed an error');

      return res.json({
        status: 10,
        error: 'The validation of the data showed an error'
      });
    }

    const { IdFilm, Title ,Rating, Description, Summary, Type, Category,
      Duration, ReleaseYear, Serie, Seasons, Productors, ProductorsExecutives,
      Director, Creators, Cast, Country, ProductionCompany, Dates, Users, State
    } = req.body;

    const existFilm = await Film.findOne({ Title }).select('-_id Title');

    if (existFilm) {
      log.warning('The film is already registered');

      return res.json({
        status: 10,
        error: 'The film is already registered'
      });
    }

    const resultId = await Film.find().select('-_id IdFilm').sort({IdUser: -1}).limit(1);
    const newIdFilm = (resultId.length === 0) ? 1 : parseInt(resultId[0].IdFilm + 1);

    const film = new Film({
      IdFilm, Title, Rating, Description, Summary, Type, Category,
      Duration, ReleaseYear, Serie, Seasons, Productors, ProductorsExecutives,
      Director, Creators, Cast, Country, ProductionCompany, Dates, Users, State
    })

    Film.IdFilm = newIdFilm;
    film.Dates.Registration.Date = globalfunctions.getDateISO();
    film.Dates.Registration.Id = globalfunctions.getIdDate();

    const responseFilmCreate = await film.save();

    if (!responseFilmCreate) {
      log.error('An error occurred when registering film');

      return res.json({
        status: 10,
        error: 'An error occurred when registering film'
      });
    }

    log.success('The film was inserted successfully');
    res.json({
      status: 1,
      data: responseFilmCreate
    });

  } catch (error) {
    log.error(`The following error occurred: ${error.message}`);
    res.json({
      status: 10,
      error: error.message
    });
  }
});

router.put('/:id', async (req, res) => {
  try {
    const { title, description } = req.body;
    const updateTask = { title, description };
    const response = await Task.findByIdAndUpdate(req.params.id, updateTask);
    console.log('response', response);
    res.json({
      status: 1,
      data: response
    });
  } catch (error) {
    res.json({
      status: 10,
      error: error
    });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const response = await Task.findByIdAndRemove(id);
    res.json({
      status: 1,
      data: response
    });
  } catch (error) {
    res.json({
      status: 10,
      error: error
    });
  }
});

module.exports = router;