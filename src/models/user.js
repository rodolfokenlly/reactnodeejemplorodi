const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
  IdUser:     { type: Number },
  Username:   { type: String, unique: true, required: true },
  Email:      { type: String, unique: true, required: true },
  Password:   { type: String, required: true },
  Firstname:  { type: String, required: true },
  Lastname:   { type: String, required: true },
  Facebook:   { type: String, required: true },
  Dates:      { type: Object, required: true },
  Users:      { type: Object, required: true },
  State:      { type: Object, required: true }
}, { versionKey: false });

UserSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', UserSchema);