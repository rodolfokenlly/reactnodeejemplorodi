//#region Variables
const mongoose = require('mongoose');
const { Schema } = mongoose;
const options = { versionKey: false };
//#endregion

//#region Entidades
const UsuarioSchema = new Schema({
    IdUser:     { type: Number },
    Username:   { type: String, unique: true, required: true },
    Email:      { type: String, unique: true, required: true },
    Password:   { type: String, required: true },
    Firstname:  { type: String, required: true },
    Lastname:   { type: String, required: true },
    Facebook:   { type: String, required: true },
    Dates:      { type: Object, required: true },
    Users:      { type: Object, required: true },
    State:      { type: Object, required: true }
}, options);

const PersonalShema = new Schema({
    IdPersonal: { type: Number },
    Codigo: { type: Number },
    Pais: { type: Object },
    Carnet: { type: String },
    CodigoGenerado: { type: Number },
    CarnetLegados: { type: String },
    Apellidos: { type: String },
    Nombres: { type: String },
    TipoDocumento: { type: Object },
    Identificacion: { type: String },
    Direccion: { type: String },
    FechaNacimiento: { type:  String },
    Email: { type: String },
    Genero: { type: Object },
    FechaRegistro: { type: String },
    LicenciaConducir: { type: String },
    Usuario: { type:  Object},
    ContratistaHistorial: { type: Array },
    Estado: { type: Number }
}, options);
//#endregion

//#region Set Configuracion Entidades
UsuarioSchema.set('toJSON', { virtuals: true });
PersonalShema.set('toJSON', { virtuals: true });
//#endregion

//#region Modulos de Exportacion
module.exports = {
    Personal: mongoose.model('Personal', PersonalShema),
    Usuario: mongoose.model('User', UsuarioSchema)
}
//#endregion