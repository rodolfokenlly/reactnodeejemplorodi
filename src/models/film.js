const mongoose = require('mongoose');
const { Schema } = mongoose;

const FilmSchema = new Schema({
  IdFilm:       { type: Number, require: true },
  Title:        { type: String, required: true },
  Rating:       { type: Number, required: true },
  Description:  { type: String, required: true },
  Summary:      { type: String, required: true },
  Type:         { type: Object, required: true },
  Category:     { type: Object, required: true },
  Duration:     { type: Object, required: true },
  ReleaseYear:  { type: Number, required: true },
  Serie:        { type: Boolean, required: true },
  Seasons:      { type: Array, required: true },
  Productors:   { type: Array, required: true },
  ProductorsExecutives: { type: Array, required: true },
  Director:     { type: String, required: true },
  Creators:     { type: Array, required: true },
  Cast:         { type: Array, required: true },
  Country:      { type: String, required: true },
  ProductionCompany: { type: String, required: true },
  Dates:        { type: Object, required: true },
  Users:        { type: Object, required: true },
  State:        { type: Object, required: true }
}, { versionKey: false });

FilmSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Film', FilmSchema);